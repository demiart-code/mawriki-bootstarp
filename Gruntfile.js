module.exports = function(grunt) {
	var gc = {
		template: 'dist/',
		tasks: [
			/*
			*  Старт
			*/
			'notify:watch',
			/*
			* Проверка JS на ошибки
			*/
			'jshint',
			/*
			*  Объединяем файлы
			*/
			'concat',
			/*
			*  Минимизируем JS
			*/
			'uglify',
			/*
			*  Оптимизация изображений
			*/
			'imagemin',
			'tinyimg',
			/*
			*  Компилируем less
			*/
			'less',
			/*
			*  Браузерные префиксы
			*/
			'autoprefixer',
			/*
			*  Группируем медиа запросы и переносим вниз файла
			*/
			'group_css_media_queries',
			/*
			*  Удаляем комментарии из CSS файлов
			*/
			'replace',
			/*
			*  Минимизируем CSS
			*/
			'cssmin',
			/*
			*  Копируем нужные файлы
			*/
			'copy',
			/*
			*  Компилируем HTML
			*/
			'pug',
			/*
			*  Завершение
			*/
			'notify:done'
		]
	};
	require('load-grunt-tasks')(grunt);
	require('time-grunt')(grunt);
	grunt.initConfig({
		globalConfig : gc,
		pkg : grunt.file.readJSON('package.json'),
		jshint: {
			options: {
				expr: true,
				jshintrc: true,
				scripturl: true
			},
			src: [
				'src/js/*.js'
			]
		},
		concat: {
			options: {
				separator: "\n",
			},
			appjs: {
				src: [
					'bower_components/jquery/dist/jquery.js',
					/* Далее плагины, которые будем использовать в проекте.
					*  Всё объединяем в один файл. Лучше всего брать не минифицированные
					*/
					
					/*
					* Bootstarp
					*/
					'bower_components/bootstrap/dist/js/bootstrap.js',
				],
				dest: 'test/js/appjs.js'
			},
			main: {
				src: [
					/*
					*  Файл проекта
					*/
					'src/js/main.js'
				],
				dest: 'test/js/main.js',
			},
		},
		uglify: {
			app: {
				options: {
					sourceMap: false,
					compress: true
				},
				files: [
					{
						expand: true,
						flatten : true,
						src: [
							'test/js/appjs.js'
						],
						dest: '<%= globalConfig.template %>/js/',
						filter: 'isFile'
					}
				]
			},
			js: {
				options: {
					sourceMap: false,
					compress: true
				},
				files: [
					{
						expand: true,
						flatten : true,
						src: [
							'test/js/main.js'
						],
						dest: '<%= globalConfig.template %>/js/',
						filter: 'isFile'
					}
				]
			}
		},
		imagemin: {
			base: {
				options: {
					optimizationLevel: 3,
					svgoPlugins: [
						{
							removeViewBox: false
						}
					]
				},
				files: [
					{
						expand: true,
						flatten : true,
						src: [
							'src/images/*.{png,jpg,gif,svg}'
						],
						dest: 'test/images/',
						filter: 'isFile'
					}
				],
			}
		},
		tinyimg: {
			dynamic: {
				files: [
					{
						expand: true,
						cwd: 'test/images', 
						src: ['**/*.{png,jpg,jpeg,svg}'],
						dest: '<%= globalConfig.template %>/images/'
					}
				]
			}
		},
		less: {
			css: {
				options : {
					compress: true,
					ieCompat: false
				},
				files : {
					'test/css/main.css' : [
						'src/less/main.less'
					]
				}
			}
		},
		autoprefixer:{
			options: {
				browsers: ['last 2 versions', 'Android 4', 'ie 8', 'ie 9', 'Firefox >= 27', 'Opera >= 12.0', 'Safari >= 6'],
				cascade: true
			},
			css: {
				files: {
					'test/css/prefix/main.css' : ['test/css/main.css']
				}
			},
		},
		group_css_media_queries: {
			group: {
				files: {
					'test/css/media/main.css': ['test/css/prefix/main.css']
				}
			}
		},
		replace: {
			dist: {
				options: {
					patterns: [
						{
							match: /\/\* *(.*?) *\*\//g,
							replacement: ' '
						}
					]
				},
				files: [
					{
						expand: true,
						flatten : true,
						src: [
							'test/css/media/*.css'
						],
						dest: '<%= globalConfig.template %>/css/',
						filter: 'isFile'
					}
				]
			}
		},
		cssmin: {
			options: {
				mergeIntoShorthands: false,
				roundingPrecision: -1
			},
			minify: {
				files: {
					'<%= globalConfig.template %>/css/main.min.css' : ['<%= globalConfig.template %>/css/main.css'],
				}
			}
		},
		pug: {
			files: {
				options: {
					pretty: '\t',
					separator:  '\n'
				},
				files: {
					"index.html": ['src/pug/index.pug'],
				}
			}
		},
		copy: {
			fonts: {
				expand: true,
				cwd: 'src/fonts',
				src: [
					'**.{ttf,svg,eot,otf,woff,woff2}',
					'bower_components/bootstrap/dist/fonts/*.*'
				],
				dest: '<%= globalConfig.template %>/fonts/',
			},
			bs: {
				expand: true,
				cwd: 'bower_components/bootstrap/dist/fonts',
				src: [
					'**.{ttf,svg,eot,otf,woff,woff2}',
				],
				dest: '<%= globalConfig.template %>/fonts/',
			}
		},
		notify: {
			watch: {
				options: {
					title: "<%= pkg.name %> v<%= pkg.version %>",
					message: 'Запуск',
					image: __dirname+'\\src\\notify.png'
				}
			},
			done: {
				options: { 
					title: "<%= pkg.name %> v<%= pkg.version %>",
					message: "Успешно Завершено",
					image: __dirname+'\\src\\notify.png'
				}
			}
		},
		watch: {
			options: {
				livereload: true,
			},
			compile: {
				files: [
					'src/**/*.*'
				],
				tasks: gc.tasks
			}
		}
	});
	grunt.registerTask('dev',		['watch']);
	grunt.registerTask('default',	gc.tasks);
};